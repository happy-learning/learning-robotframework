# learning-robotframework
## Preconditions
* download webdriver
  - [download webdrivers](https://seleniumhq.github.io/selenium/docs/api/py/index.html#drivers)
  - copy webdriver to environment `PATH`
* install libraries
```bash
  $ python3 -m pip install robotframework
  $ python3 -m pip install robotframework-seleniumlibrary
```
* Starting demo application
```bash
  $ python demoapp/server.py
```
## Running tests
* Test that everything works:
```bash
  $ python3 -m robot web-demo/login_tests
  $ python3 -m robot api-demo/sample_tests
```