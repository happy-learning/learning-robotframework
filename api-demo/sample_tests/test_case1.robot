*** Settings ***
Documentation     api test demo
Library           Collections
Library           RequestsLibrary

*** Test Cases ***
Get Requests
    Create Session    api   https://api.github.com      disable_warnings=1
    ${resp}   Get Request   api    /users/bulkan
    Log   ${resp.json()}
    Should Be Equal As Strings	${resp.status_code}	200	
    Dictionary Should Contain Value	${resp.json()}	Bulkan Evcimen	
